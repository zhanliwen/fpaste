Name:           fpaste
Version:        0.4.0.1
Release:        1
Summary:        A simple paste util
License:        GPLv3+
URL:            https://pagure.io/%{name}
Source0:        https://pagure.io/releases/%{name}/%{name}-%{version}.tar.gz

BuildArch:      noarch
Requires:       python3

%description
It allows easy uploading of multiple files, or of copy&pasted text from stdin,
without requiring a web browser. A unique fpaste link is returned, which can
then be given to others who are offering help.

%package_help

%prep
%autosetup

%build

%install
mkdir -p %{buildroot}%{_bindir}
make install BINDIR=%{buildroot}%{_bindir} MANDIR=%{buildroot}%{_mandir}

%pre

%preun

%post

%postun

%files
%defattr(-,root,root)
%license COPYING
%{_bindir}/%{name}

%files help
%doc README.rst TODO
%{_mandir}/man1/%{name}.1.gz

%changelog
* Mon Jul 27 2020wenzhanli<wenzhanli2@huawei.com> - 0.4.0.1-1
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:version update 0.4.0.1

* Fri Oct 11 2019 openEuler Buildteam <buildteam@openeuler.org> - 0.4.0.0-1
- Package init
